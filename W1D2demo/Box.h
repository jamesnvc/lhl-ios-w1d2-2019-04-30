//
//  Box.h
//  W1D2demo
//
//  Created by James Cash on 30-04-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Box : NSObject {
    NSInteger _height;
}

- (NSInteger)height;
- (void)setHeight:(NSInteger)newHeight;

//@property (nonatomic,assign) NSInteger height;
@property (nonatomic,assign) NSInteger width;
@property (nonatomic,assign) NSInteger depth;


+ (instancetype)box;
- (instancetype)initWithHeight:(NSInteger)h width:(NSInteger)w depth:(NSInteger)d;
- (NSInteger)volume;
- (CGFloat)cost;

@end

NS_ASSUME_NONNULL_END
