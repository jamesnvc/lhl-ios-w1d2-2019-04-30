//
//  Box.m
//  W1D2demo
//
//  Created by James Cash on 30-04-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "Box.h"

@implementation Box

- (instancetype)initWithHeight:(NSInteger)h width:(NSInteger)w depth:(NSInteger)d {
    self = [super init];
    if (self) {
        _height = h;
        _width = w;
        _depth = d;
    }
    return self;
}

+ (instancetype)box {
    return [[Box alloc] initWithHeight:1 width:1 depth:1];
}

- (NSInteger)height {
    return _height;
}

- (void)setHeight:(NSInteger)newHeight {
    _height = newHeight;
//    self.height = newHeight; -> [self setHeight:newHeight]
}

- (NSInteger)volume {
    // [self height]; // self.height
    return self.width * self.height * self.depth;
}

- (CGFloat)cost {
    return (5.0 * self.depth) + self.width + self.height;
}

@end
