//
//  main.m
//  W1D2demo
//
//  Created by James Cash on 30-04-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Box.h"

int doStuff(float x, char c) {
    x = x * 10;
    NSLog(@"Inside dostuff %f %c", x, c);
    return 42;
}

int doStuff2(float *x, char c) {
    NSLog(@"x = %p *x = %f", x, *x);
    *x = *x * 10;
    NSLog(@"Inside dostuff %f %c", *x, c);
    return 42;
}

// DON'T DO THIS!!!!
int *badFunction() {
    int foo = 10;
    return &foo;
}

NSDate *okayFunction() {
    NSDate *d = [[NSDate alloc] init];
    //    NSDate *d = [NSDate new];
//    NSDate *d = [NSDate date];
//    [NSString string]
    return d;
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSDate *date = [[NSDate alloc] init];
        NSInteger someInt = 5;
        char* h1 = "helπτθαγβγγὄ퇴lo";
        NSString *h2 = @"hello";
        [h2 length];
        [h2 lengthOfBytesUsingEncoding:NSUTF8StringEncoding];

        for (int i = 0; i < 10; i++) {
            NSLog(@"oeu %d", i);
        }

        NSArray* strings = @[@"a", @"b", @"c"];
        for (int i  = 0; i < strings.count; i++) {
            NSLog(@"%d: %@", i, strings[i]);
        }
        for (NSString *s in strings) {
            NSLog(@"%@", s);
        }
        [strings enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop) {
            NSLog(@"%ld: %@", idx, obj);
        }];

        int i = 0;
        while (i < 3) {
            i++;
        }
        do {
            i++;
        } while (i < 5);
        switch (i) {
            case 1:
                NSLog(@"i is one");
                break;
            case 2+3:
                break;
            default:
                break;
        }


        float x; // make space for me to store a float & let me refer to that float as 'x'
        x = 10.0; // put the float value 10.0 in the space that I'm calling 'x'
        NSLog(@"&x = %p", &x);
        int r = doStuff(x, 'c'); // pass the *value* stored at the location 'x' into the function doStuff
        NSLog(@"x after doStuff = %f", x);
        int r2 = doStuff2(&x, 'c');
        NSLog(@"x after doStuff2 = %f", x);
        NSLog(@"&r2 = %p", &r2);

        // DON'T DO THIS!!!
        int* f = badFunction();
        doStuff(100.0, 'd');
        *f = 100;
        NSLog(@"f = %d", *f);
        // end don't do this

        // why objects?
        NSInteger box1Width = 10;
        NSInteger box1Height = 5;
        NSInteger box1Depth = 3;

        NSInteger box2Width = 15;
        // ..

        NSInteger box1Volume = box1Width * box1Height * box1Depth;
        NSLog(@"box 1 volume = %ld", box1Volume);

        Box* box1 = [[Box alloc] init];
        box1.height = 10;
        box1.width = 15;
        box1.depth = 5;
        NSLog(@"Box 1 volume = %ld", [box1 volume]); // box1.volume

        Box* box2 = [[Box alloc] initWithHeight:5 width:5 depth:5];
        NSLog(@"Box 2 vol = %ld cost = %f", box2.volume, box2.cost);

        NSLog(@"newbox vol = %ld", [[Box box] volume]);
    }
    return 0;
}
